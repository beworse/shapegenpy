### Założenia projektu:
[]**Główne**
- [x] Dynamiczne wczytywanie modułów.
- [] Kombatybilność *WINDOWS* / *LINUX*.
- [] Obiekty tylko 2D.
- [] Kod ma być binarką.

[]**Poboczne**
- [x] Nie wykorzystywać **print** jako logowanie, zmiast tego użyć gotowej bilioteki.
- [x] Kontrola wersji.
- [x] Tagowanie rzeczy do zmiany **TODO: *co nalezy zrobic***.
- [] Stała dokumentacja kodu.
- [] Testy jednostkowe.
- [] Zadbać o informacje o wersjach pakietów.

### Wykorzystane biblioteki
1. [logging](https://realpython.com/python-logging/), dla lepszych logow
1. argsparse - proste parsowanie parametrow wejsciowych
1. importlib - dynamiczne wczytywanie plikow
1. os, sys - kompatybilnosc z innym systemem operacyjnym

### Przydatne linki
1. [python - dokumentowanie kodu w stylu numby style](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_numpy.html)
1. [python - błędy](https://docs.python.org/3/library/exceptions.html)
1. [poradnik makrdow](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)