#!/usr/bin/env python3
import os
import sys
import logging
import argparse
from importlib import import_module

#TODO: Write test functions
def TestModule(obj):
    """
        Test if object contatins some functions

        Return
        True/False
    """
    if obj is None:
        return False
    return True 

#TODO: Check with other dirs
def LoadModules(modules_dir=""):
    """
        Load dynamic shapes and check them
    """

    # load default path
    if not modules_dir:
        modules_dir = os.path.dirname(os.path.realpath(__file__))
        modules_dir = os.path.join(modules_dir, "shapes")
    
    # checking directory
    if not os.path.exists(modules_dir):
        logging.critical("Given directory %s not exists" % modules_dir)
        raise NotADirectoryError
    module_base = os.path.basename(modules_dir)

    # modules to load
    modules_names = [fname for fname in os.listdir(modules_dir) if fname.endswith('.py')]
    
    # prepare list of modules
    if "__init__.py" in modules_names:
        logging.info("Ignoring file ... __init__.py")
        modules_names.remove("__init__.py")
    modules_names = [os.path.splitext(x)[0] for x in modules_names]

    # dynamic load modules
    loaded_shapes = {}
    for module_name in modules_names:
        # loop for easier continue while try fail
        while True: 
            # import module
            try:
                logging.info("Trying to load module \'%s\'." % module_name )
                module = import_module(module_base + "." + module_name)
            except Exception as e:
                logging.warning("Module \'%s\' failed to import, because of error: %s." % (module_name, e))
                break
            
            # checking if module has right class
            try:
                obj = getattr(module, module_name)()
            except AttributeError:
                logging.warning("AttributeError after trying to call method %s from module %s that is located at:" % (module_name, modules_dir) )
                break
            
            # checking if obj has right functions
            if TestModule(obj):
                logging.info("Module \'%s\' loaded with success" % module_name)
                loaded_shapes[module_name] = obj
            else:
                logging.warning("Module \'%s\' ignored, beacause of test fail." % module_name )
            break
        
    if not loaded_shapes:
        logging.critical("No module from dir %s loaded with success" % modules_dir)
        raise ImportError("No module from dir %s loaed with success." % modules_dir)
    
    return loaded_shapes

if __name__ == "__main__":
    # parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-d','--debug_level', type=int, default=logging.DEBUG, help='logging library debug level')
    parser.add_argument('-m','--modules_dir', type=str, default="", help='directory that contain shape modules')
    args =parser.parse_args()
    
    # setup logging
    logging.basicConfig(level=args.debug_level)
    logging.info("Program started with: %s" % str(args))

    # dynamic load libraries
    shapes = LoadModules(modules_dir=args.modules_dir)